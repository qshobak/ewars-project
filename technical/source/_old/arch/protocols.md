# Protocols

The applications within the EWARS ecosystem have been set up to use varying protocols in order to sync data to/from remote devices and inter-account through a number of primary and fallback mechanisms.

<link rel="stylesheet" type="text/css" href="/static/mermaid/mermaid.css">
<script src="/static/mermaid/mermaid.min.js"></script>
<script>mermaid.initialize({startOnLoad:true});</script>


## Component Support

### Mobile

<div class="mermaid">
sequenceDiagram
    Mobile->>Server: SMS
    Mobile->>Desktop: SMS;
    Mobile->>Desktop: Bluetooth;
    Mobile->>Desktop: TCP;
    Mobile->>Server: TCP;
    Mobile->>Server: HTTPS;
    Mobile->>Hub: STUN;
    Hub->>Server: TCP;
    Mobile->>Mobile: QR;
    Mobile->>Desktop: QR;
    Mobile->>Mobile: Bluetooth;
    Mobile->>Mobile: AndroidWifi;
    Mobile->>Mobile: iOSNFC;
</div>

### Desktop

<div class="mermaid">
sequenceDiagram
    Desktop->>Server: TCP;
    Desktop->>Desktop: TCP;
    Desktop->>Desktop: UDP;
    Desktop->>Mobile: SMS;
    Desktop->>Hub: STUN;
    Desktop->>Mobile: TCP;
    Desktop->>Mobile: Bluetooth;
    Hub->>Server: TCP;
</div>

### Server

<div class="mermaid">
sequenceDiagram
    Server->>Server: TCP;
    Server->>Server: HTTPS;
    Server->>Hub: TCP;
</div>

### Hub

<div class="mermaid">

</div>

## Protocol Definitions

| Name | Description | Supported On |
-------------------------------------
| TCP/IP | Low-level socket | Desktop, Server, Mobile |

### TCP/IP (Stable)

### UDP (Stable)

### HTTP(S) (Stable)

### SMS/MMS (Unstable)
 
### Bluetooth (Unstable)

### SigFox (WIP)

### LoRa (WIP)

### QR Codes (Unstable)

### JSON Export (Unstable)

### CSV Export (Stable)

### Android Wifi (Unstable)

### iOS NFC (Unstable)
