=========
Licensing
=========

The EWARS set of applications are licenced under Apache 2.0. This means that you are free to copy, distribute and use the code as you wish. However there are some stipulations attached. Any modifications that you make to the original source code, **must** be made public, you must provide your source code free of charge as well as any instructions required for building the derivative work.

For more information about the GPLV3 licence and what is required of you as an organization, developer or otherwise please read the GPL licence included in the code repository in full and direct any questions to [support@ewars.ws](mailto:support@ewars.ws).
