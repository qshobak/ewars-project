libewars (libewars)
=====================

.. contents::
   :numbered:
   :local:

Overview
---------

`libewars` is a Rust-based library shared amongst the other component of the system. It implements most if not all of the business lgoic and processes required in order for the platform to operate. 

`libewars` is exposed to other components of the system either through direct compilation of the library into the application, or compilation through FFI and a binding generator for the specific target language. For instance, the Web Client component depends on a build of the `libewars` library compiled using WASm compilation targets, the desktop application depends on libewars through an FFI and c binding generator.

Structure
----------

The library itself is responsible for the following functions:

#. Reporting Engine
#. Alarms Engine
#. Tasks Engine
#. Utilities modules
#. Permissions
#. Data storage, retrieval and manipulation
#. Asynchronous processes such as Hooks, Alarm evaluation and Event Log Streams

Build Flags
------------

libewars uses build flags in order to conditionally build portions of the library depending on the component utilizing the library. For instance, WASM based builds do not required the database connectivity features, asynchronous actors, etc... these items are dropped frmo the compilation when compiling to WASM by specifying feature flags in the target compontns build process.


